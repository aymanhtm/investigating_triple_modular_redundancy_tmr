#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>

    /* macros & typedefs */
#define ASSERT(c,err)	do { \
							if(!(c))\
							{\
								FATAL_ERROR(err,__FILE__,__LINE__);\
							}\
						}while(0)
#define N	2 /* rows of the NxM matrix */
#define M	3 /* columns of the NxM matrix */
#define R   4 /* columns of MxR matrix */

typedef	unsigned long TYPE; /* type of data entry */
typedef struct data	/* data passed to threads */
{
    TYPE a;
    TYPE b;
	TYPE res;
}data;

    /* static functions */
void * pthread_subroutine (void * param);
void reading_matrix (const char* filename, void*** matrix_ptr, int n, int m);
void freeArray (void **a, int m);
int voter (TYPE elt1, TYPE elt2, TYPE elt3);
void PrintErrReport ();
void RandFaultInjection ();
int main ();
void FATAL_ERROR (const char* msg, const char* file, int line);

	/* globlal variables */
long openthreads = 0;
pthread_mutex_t x;	/* mutex for the usage of global variables & malloc among threads */
pthread_cond_t c;	/* conditional variable for efficiency */
TYPE ErrCount_mod1 = 0, ErrCount_mod2 = 0, ErrCount_mod3 = 0;
data **THdata1, **THdata2, **THdata3;

/* -------------------------------------------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------------------------------------------- */

	/* free-ing two-dimensional arrays */
void freeArray (void **a, int m)
{
    int i;

    for (i = 0; i < m; ++i) 
	{
        free(a[i]);
    }
    free(a);
}

/* -------------------------------------------------------------------------------------------------------------- */

	/* random fault injection */	
void RandFaultInjection ()
{
	int i,j;
	static int t = 0;

	i = (int) rand()%(N*R);
	j = (int) rand()%M;

	pthread_mutex_lock(&x);
	if ((t%3) == 0)
	{
        THdata1[0][j].res++;	
	}else if ((t%3) == 1)
	{
		THdata2[i][j].res++;	
	}else
	{
		THdata3[i][j].res++;	
	}

	t++;

	if (t == 3)
	{
		THdata1[i][j].res+=1;
		THdata2[i][j].res+=3;
		THdata3[i][j].res+=5;
	}
	pthread_mutex_unlock(&x);
	
}

/* -------------------------------------------------------------------------------------------------------------- */

	/* prints to stdout the status of errors */
void PrintErrReport ()
{
	fprintf(stdout,"\n\n------------	ERROR COUNTING    ------------\n");
	fprintf(stdout,"MODULE_1:	%lu\n",ErrCount_mod1);
	fprintf(stdout,"MODULE_2:	%lu\n",ErrCount_mod2);
	fprintf(stdout,"MODULE_3:	%lu\n",ErrCount_mod3);
	fprintf(stdout,"----------------------------------------------\n\n\n");
}

/* -------------------------------------------------------------------------------------------------------------- */

	/* voter function */
int voter (TYPE elt1, TYPE elt2, TYPE elt3)
{
	int err = -1;

	if ((elt1 == elt2) && (elt1 == elt3))
	{
		err = 0;	
	}else if (elt1 == elt2)
	{
		pthread_mutex_lock(&x);
		ErrCount_mod3++;
		pthread_mutex_unlock(&x);
		err = 3;
	}else if (elt1 == elt3)
	{
		pthread_mutex_lock(&x);
		ErrCount_mod2++;
		pthread_mutex_unlock(&x);
		err = 2;
	}else if (elt2 == elt3)
	{
		pthread_mutex_lock(&x);
		ErrCount_mod1++;
		pthread_mutex_unlock(&x);
		err = 1;
	}else
	{
		pthread_mutex_lock(&x);
		ErrCount_mod1++;
		ErrCount_mod2++;
		ErrCount_mod3++;
		pthread_mutex_unlock(&x);
		err = -1;
	}
	
	return err;
}

/* -------------------------------------------------------------------------------------------------------------- */

	/* reading the matrices function */
void reading_matrix (const char* filename, void*** matrix_ptr, int n, int m)
{
	TYPE temp;
	int i, j;
	FILE *fp;
	
	pthread_mutex_lock(&x);
	(*matrix_ptr) = (TYPE**) malloc(sizeof(TYPE*)*n);
	ASSERT((*matrix_ptr) != NULL, "allocating A/B");

	for(i = 0; i < n; i++)
	{
		(*matrix_ptr)[i] = (TYPE*) malloc(sizeof(TYPE)*m);
		ASSERT((*matrix_ptr)[i] != NULL, "allocating A/B inside the loop");
	}
	pthread_mutex_unlock(&x);
	
	fp = fopen(filename,"r");
	//ASSERT(fp != NULL, "opening the file of data for A/B");

	for(i = 0; i < n; i++)
	{
		for(j = 0; j < m; j++)
		{
			fscanf(fp,"%lu",&temp);
			if( feof(fp) ) break;
			( (TYPE**) *matrix_ptr)[i][j] = temp;
		}
		if( feof(fp) ) break;
	}
	fclose(fp);
}

/* -------------------------------------------------------------------------------------------------------------- */

	/* pthread subtroutine */
void * pthread_subroutine (void * param)
{
	data *p = (data*) param;
	p->res = p->a*p->b;

		/* decrementing the number of openthreads */
	pthread_mutex_lock(&x);
	openthreads--;
	pthread_cond_signal(&c);
	pthread_mutex_unlock(&x);

	pthread_exit(0); 
}

/* -------------------------------------------------------------------------------------------------------------- */

	/* error function */
void FATAL_ERROR (const char* msg, const char* file, int line)
{
	fprintf(stderr,"\n\n------>	FATAL ERROR:	Provoked while %s from file \"%s\" at line [%d]\n\n\n",msg,file,line);
	exit(1);
}

/* -------------------------------------------------------------------------------------------------------------- */

    /* main */
int main () 
{
    	/* local variables */
	void **A, **B, **C;
	pthread_t **THid1, **THid2, **THid3;
//	data **THdata1, **THdata2, **THdata3; //considered as global for the use of fault injection function
	int i,j,k;
	int err,fatal = 0;
		/* for debugging purposes */
	TYPE temp = 0;
	FILE *fp;

    	/* program body */

		/* initialization of the mutex & conditional */
	pthread_mutex_init(&x,NULL);
	pthread_cond_init(&c,NULL);

		/* reading the matrices A & B */
	reading_matrix("matrix_A.dat",&A,N,M);
	reading_matrix("matrix_B.dat",&B,M,R);

	pthread_mutex_lock(&x);
		/* memory allocation for THid1 */
	THid1 = (pthread_t**) malloc(sizeof(pthread_t*)*N*R);
	ASSERT(THid1 != NULL, "allocating N*R THid1");
		/* memory allocation for THid2 */
 	THid2 = (pthread_t**) malloc(sizeof(pthread_t*)*N*R);
	ASSERT(THid2 != NULL, "allocating N*R THid2");
		/* memory allocation for THid3 */
	THid3 = (pthread_t**) malloc(sizeof(pthread_t*)*N*R);
	ASSERT(THid3 != NULL, "allocating N*R THid3");

		/* memory allocation for THdata1 */
	THdata1 = (data**) malloc(sizeof(data*)*N*R);
	ASSERT(THdata1 != NULL, "allocating NxR THdata1");
		/* memory allocation for THdata2 */
	THdata2 = (data**) malloc(sizeof(data*)*N*R);
	ASSERT(THdata2 != NULL, "allocating NxR THdata2");
		/* memory allocation for THdata3 */
	THdata3 = (data**) malloc(sizeof(data*)*N*R);
	ASSERT(THdata3 != NULL, "allocating NxR THdata3");
		/* memory allocation for the 2nd dimension of both THid & THdata */
	for(i = 0; i < N*R; i++)
	{
			/* memory allocation for THid1 */
		THid1[i] = (pthread_t*)malloc(sizeof(pthread_t)*M);
		ASSERT(THid1[i] != NULL, "allocating M THid1 inside the NxR loop");
			/* memory allocation for THid2 */
		THid2[i] = (pthread_t*)malloc(sizeof(pthread_t)*M);
		ASSERT(THid2[i] != NULL, "allocating M THid2 inside the NxR loop");
			/* memory allocation for THid3 */
		THid3[i] = (pthread_t*)malloc(sizeof(pthread_t)*M);
		ASSERT(THid3[i] != NULL, "allocating M THid3 inside the NxR loop");

			/* memory allocation for THdata1 */
		THdata1[i] = (data*)malloc(sizeof(data)*M);
		ASSERT(THdata1[i] != NULL, "allocating M THdata1 inside the NxR loop");
			/* memory allocation for THdata2 */
		THdata2[i] = (data*)malloc(sizeof(data)*M);
		ASSERT(THdata2[i] != NULL, "allocating M THdata2 inside the NxR loop");
			/* memory allocation for THdata3 */
		THdata3[i] = (data*)malloc(sizeof(data)*M);
		ASSERT(THdata3[i] != NULL, "allocating M THdata3 inside the NxR loop");
	}
	pthread_mutex_unlock(&x);

	for (i = 0; i < N; i++)
	{
		for (j = 0; j < R; j++)
		{
			for (k = 0; k < M; k++)
			{
					/* assignment of data of 1st calculation */
				THdata1[i*R+j][k].a = ((TYPE**) A)[i][k];
				THdata1[i*R+j][k].b = ((TYPE**) B)[k][j];
					/* assignment of data of 2nd calculation */
				THdata2[i*R+j][k].a = ((TYPE**) A)[i][k];
				THdata2[i*R+j][k].b = ((TYPE**) B)[k][j];
					/* assignment of data of 2nd calculation */
				THdata3[i*R+j][k].a = ((TYPE**) A)[i][k];
				THdata3[i*R+j][k].b = ((TYPE**) B)[k][j];

					/* creating the threads for the 1st calculation */
				err = pthread_create(&THid1[i*R+j][k],NULL,pthread_subroutine,(void *)&THdata1[i*R+j][k]);
				ASSERT(err == 0, "creating the thread for the 1st calculation inside the k loop");
					/* creating the threads for the 2nd calculation */
				err = pthread_create(&THid2[i*R+j][k],NULL,pthread_subroutine,(void *)&THdata2[i*R+j][k]);
				ASSERT(err == 0, "creating the thread for the 2nd calculation inside the k loop");
					/* creating the threads for the 3rd calculation */
				err = pthread_create(&THid3[i*R+j][k],NULL,pthread_subroutine,(void *)&THdata3[i*R+j][k]);
				ASSERT(err == 0, "creating the thread for the 3rd calculation inside the k loop");

					/* incrementing the number of openthreads */
				pthread_mutex_lock(&x);
				openthreads += 3;
				pthread_mutex_unlock(&x);
			}	
		}
	}
		/* allocating memory for C */
	pthread_mutex_lock(&x);
	C = (TYPE**) malloc(sizeof(TYPE*)*N);
	ASSERT(C != NULL, "allocating N in 1st dimnesion of C");
	for ( i = 0; i < N; i++)
	{
		C[i] = (TYPE*)malloc(sizeof(TYPE)*R);
		ASSERT(C[i] != NULL, "allocating R in 2nd dimension of C inside the N loop");
	}
	pthread_mutex_unlock(&x);

		/* locking the main thread until all others are done */
	pthread_mutex_lock(&x);
	while(openthreads>0)
		pthread_cond_wait(&c,&x);
	pthread_mutex_unlock(&x);

//			DEBUGGGG		----------------------------------------------------- // the commented lines can be completely neglected
//		/* debugging */
//	fp = fopen("temp.dat","w");
//	ASSERT(fp != NULL, "opening the file temp.dat");
//		/* showing A */
//	fprintf(fp,"A =\n");
//	for (i = 0; i < N; i++)
//	{
//		fprintf(fp,"\t");
//		for (j = 0; j < M; j++)
//		{
//			fprintf(fp,"%lu\t",((TYPE**) A)[i][j]);
//		}
//		fprintf(fp,"\n");
//	}
//		/* showing B */
//	fprintf(fp,"B =\n");
//	for (i = 0; i < M; i++)
//	{
//		fprintf(fp,"\t");
//		for (j = 0; j < R; j++)
//		{
//			fprintf(fp,"%lu\t",((TYPE**) B)[i][j]);
//		}
//		fprintf(fp,"\n");
//	}
//		/* showing C1 */
//	fprintf(fp,"C1 =\n");
//	for (i = 0; i < N; i++)
//	{
//		fprintf(fp,"\t");
//		for (j = 0; j < R; j++)
//		{
//			for (k = 0; k < M; k++)
//			{
//				//temp = THdata1[i*R+j][k].res;
//				//fprintf(fp,"%lu\t",temp);
//				temp += THdata1[i*R+j][k].res;
//			}
//			fprintf(fp,"%lu\t",temp);
//			temp = 0;	
//		}
//		fprintf(fp,"\n");	
//	}
//		/* showing C2 */
//	fprintf(fp,"C2 =\n");
//	for (i = 0; i < N; i++)
//	{
//		fprintf(fp,"\t");
//		for (j = 0; j < R; j++)
//		{
//			for (k = 0; k < M; k++)
//			{
//				//temp = THdata1[i*R+j][k].res;
//				//fprintf(fp,"%lu\t",temp);
//				temp += THdata2[i*R+j][k].res;
//			}
//			fprintf(fp,"%lu\t",temp);
//			temp = 0;	
//		}
//		fprintf(fp,"\n");	
//	}
//		/* showing C3 */
//	fprintf(fp,"C3 =\n");
//	for (i = 0; i < N; i++)
//	{
//		fprintf(fp,"\t");
//		for (j = 0; j < R; j++)
//		{
//			for (k = 0; k < M; k++)
//			{
//				//temp = THdata1[i*R+j][k].res;
//				//fprintf(fp,"%lu\t",temp);
//				temp += THdata3[i*R+j][k].res;
//			}
//			fprintf(fp,"%lu\t",temp);
//			temp = 0;	
//		}
//		fprintf(fp,"\n");	
//	}
//	fclose(fp);

	while(1)
	{
			/* showing A */
		fprintf(stdout,"A =\n");
		for (i = 0; i < N; i++)
		{
			fprintf(stdout,"\t");
			for (j = 0; j < M; j++)
			{
				fprintf(stdout,"%lu\t",((TYPE**) A)[i][j]);
			}
			fprintf(stdout,"\n");
		}
			/* showing B */
		fprintf(stdout,"B =\n");
		for (i = 0; i < M; i++)
		{
			fprintf(stdout,"\t");
			for (j = 0; j < R; j++)
			{
				fprintf(stdout,"%lu\t",((TYPE**) B)[i][j]);
			}
			fprintf(stdout,"\n");
		}
			/* showing C1 */
		fprintf(stdout,"C1 =\n");
		for (i = 0; i < N; i++)
		{
			fprintf(stdout,"\t");
			for (j = 0; j < R; j++)
			{
				for (k = 0; k < M; k++)
				{
					temp += THdata1[i*R+j][k].res;
				}
				fprintf(stdout,"%lu\t",temp);
				temp = 0;	
			}
			fprintf(stdout,"\n");	
		}
			/* showing C2 */
		fprintf(stdout,"C2 =\n");
		for (i = 0; i < N; i++)
		{
			fprintf(stdout,"\t");
			for (j = 0; j < R; j++)
			{
				for (k = 0; k < M; k++)
				{
					temp += THdata2[i*R+j][k].res;
				}
				fprintf(stdout,"%lu\t",temp);
				temp = 0;	
			}
			fprintf(stdout,"\n");	
		}
			/* showing C3 */
		fprintf(stdout,"C3 =\n");
		for (i = 0; i < N; i++)
		{
			fprintf(stdout,"\t");
			for (j = 0; j < R; j++)
			{
				for (k = 0; k < M; k++)
				{
					temp += THdata3[i*R+j][k].res;
				}
				fprintf(stdout,"%lu\t",temp);
				temp = 0;	
			}
			fprintf(stdout,"\n");	
		}
			/* putting the results in C after executing the voter for each subelement */
		fprintf(stdout,"C =\n");
		for (i = 0; i < N; i++)
		{
			fprintf(stdout,"\t");
			for (j = 0; j < R; j++)
			{
				for (k = 0; k < M; k++)
				{
					err = voter(THdata1[i*R+j][k].res,THdata2[i*R+j][k].res,THdata3[i*R+j][k].res);
					if (err == 0)
					{
						temp += THdata1[i*R+j][k].res;
					}else if (err == 1)
					{
						temp += THdata2[i*R+j][k].res;
							/* correction of the error data for the next cycle */
						THdata1[i*R+j][k].res = THdata2[i*R+j][k].res;
					}else if (err == 2)
					{
						temp += THdata1[i*R+j][k].res;
							/* correction of the error data for the next cycle */
						THdata2[i*R+j][k].res = THdata1[i*R+j][k].res;
					}else if (err == 3)
					{
						temp += THdata1[i*R+j][k].res;
							/* correction of the error data for the next cycle */
						THdata3[i*R+j][k].res = THdata1[i*R+j][k].res;
					}else
					{
							/* will lead the system to die but first we want to see the last report */
						fatal = -1;
						//break;
						//FATAL_ERROR("voting between three different values",__FILE__,__LINE__);
					}
				}
				((TYPE**) C)[i][j] = temp;
				fprintf(stdout,"%lu\t",temp);
				temp = 0;	
			}
			fprintf(stdout,"\n");	
		}
			/* for debugg purposes we sleep 1s */
		sleep(1);
			/* print of stdout the counts of errors of each module */
		PrintErrReport();

			/* inserted here in order to obtain the last error report */
		if (fatal == -1)
		{
			FATAL_ERROR("voting between three different values",__FILE__,__LINE__);
		}
			/* introduces a fault randomly in one of the modules at a time */
		RandFaultInjection();
			/* for debugg purposes we sleep 5s */
		sleep(5);
	}
		/* destroying mutex & conditional */
	pthread_mutex_destroy(&x);
	pthread_cond_destroy(&c);

		/* free-ing dynamic memory allocation */
	freeArray(A,N);
	freeArray(B,M);
	freeArray( (void **) THid1,N*R);
	freeArray( (void **) THdata1,N*R);
	freeArray( (void **) THid2,N*R);
	freeArray( (void **) THdata2,N*R);
	freeArray( (void **) THid3,N*R);
	freeArray( (void **) THdata3,N*R);
		
		/* end */
	return 0;
}

/* -------------------------------------------------------------------------------------------------------------- */